set term pdf enhanced font "Times,18" rounded linewidth 2.0
set key bottom right
set format y "%.2f"
set grid


set xlabel "p"
set ylabel "H_p"

set output "fig1.pdf"
set xrange[0.:60]
set yrange[-0.35:0.05]
plot \
"H_m1.6_fig1" u 1:2:3 title "E=-1.6" w yerrorlines ls 1,\
"H_m1.2_fig1" u 1:2:3 title "E=-1.2" w yerrorlines ls 2,\
"H_m1.0_fig1" u 1:2:3 title "E=-1.0" w yerrorlines ls 3,\
"H_m0.9_fig1" u 1:2:3 title "E=-0.9" w yerrorlines ls 4,\
"H_m0.8_fig1" u 1:2:3 title "E=-0.8" w yerrorlines ls 6,\

